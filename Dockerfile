FROM arm32v7/ubuntu:21.04
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install ssh -y
RUN usermod -p "" root
RUN echo "PermitEmptyPasswords yes" >> /etc/ssh/sshd_config
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
RUN echo "export TERM=linux" >> /root/.bashrc
CMD bash -c '/etc/init.d/ssh start && sleep infinity'
